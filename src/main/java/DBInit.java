import java.sql.*;

public class DBInit {
    static final String DB_URL = "jdbc:h2:tcp://localhost/~/test;AUTO_SERVER=TRUE";

    public static void main(String[] args) throws Exception
    {
        try
        {
            Connection conn = DriverManager.getConnection(DB_URL,"sa","");
            conn.createStatement().executeUpdate("drop table table_cols IF EXISTS");
            conn.createStatement().executeUpdate("drop table table_list IF EXISTS");

            conn.createStatement().executeUpdate("CREATE TABLE TABLE_LIST (" +
                    "TABLE_NAME VARCHAR(32) COMMENT 'имя таблицы', " +
                    "PK VARCHAR(256) COMMENT 'поля первичного ключа, разделитель - запятая')");
            conn.createStatement().executeUpdate("CREATE TABLE TABLE_COLS (" +
                    "TABLE_NAME VARCHAR(32) COMMENT 'имя таблицы', " +
                    "COLUMN_NAME VARCHAR(32) COMMENT 'имя поля', " +
                    "COLUMN_TYPE VARCHAR(32) COMMENT 'тип данных поля - INT или VARCHAR')");

            conn.createStatement().executeUpdate("INSERT " +
                    "INTO table_cols( TABLE_NAME,COLUMN_NAME,COLUMN_TYPE) VALUES" +
                    "('users','first_name','VARCHAR(32)')," +
                    "('users','second_name','VARCHAR(32)')," +
                    "('users','id','INT')," +
                    "('accounts','register_date','TIMESTAMP')," +
                    "('accounts','CARD_NUMBER','INT')," +
                    "('accounts','ACCOUNT','VARCHAR(32)')," +
                    "('accounts','ACCOUNT_ID','INT')");
            conn.createStatement().executeUpdate("INSERT " +
                    "INTO table_list(TABLE_NAME,PK) VALUES" +
                    "('users','ID')," +
                    "('accounts','account, account_id')");
            /* */
            conn.close();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        }
    }
}

