import java.io.*;
import java.sql.*;

public class DBToFileWriter {
        static final String DB_URL = "jdbc:h2:tcp://localhost/~/test;AUTO_SERVER=TRUE";

        public static void main(String[] args) throws Exception
        {
            File file = new File("database_keys.txt");
            file.createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, false));

            try
            {
                Connection conn = DriverManager.getConnection(DB_URL,"sa","");
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery("SELECT * from table_list");
                PreparedStatement stm2 = conn.prepareStatement("SELECT COLUMN_TYPE from table_cols" +
                        " where LOWER(TABLE_NAME)=? and LOWER(COLUMN_NAME)=?");

                while (rs.next()) {
                    String tableName=rs.getString(1);
                    String[] pks=rs.getString(2).split(", ");
                    String pkType;
                    stm2.setString(1,tableName.toLowerCase());
                    for (String pk : pks) {
                        if (!(rs.isFirst())) {writer.append("\n");}
                        stm2.setString(2,pk.toLowerCase());
                        ResultSet rs2=stm2.executeQuery();
                        if (rs2.next()) {
                            pkType = rs2.getString(1);
                        } else {
                            pkType = "ERROR";
                        }
                        writer.append(tableName + ", " + pk + ", " + pkType);
                    }
                }
                conn.close();

            } catch (SQLException e) {
                System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
            }

            writer.close();
        }
}

